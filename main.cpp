#include <iostream>

using namespace std;

int main()
{
    int length,temp;
    cout<<"Enter length\n";
    cin>>length;
    int array[length];
    for(int i=0 ; i<length ; i++)
    {
        cout<<"Enter array's "<<i<<" place.\n";
        cin>>array[i];
    }

    for(int i=0 ; i<length-1 ; i++)
    {
            for(int j=0 ; j<length-i-1 ; j++)
            {
                if (array[j]>array[j+1])
                {
                    temp=array[j];
                    array[j]=array[j+1];
                    array[j+1]=temp;

                }
            }
    }
    for(int i=0 ; i<length ; i++)
    {
        cout<<array[i]<<"  ";
    }
    cout<<endl;


    return 0;
}
